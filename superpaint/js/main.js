const canvas = document.getElementById("canvas");
const c = canvas.getContext("2d");
const inputs = document.querySelectorAll(".buttons");
const inputColor = document.getElementById("pickColor");
const inputWeight = document.getElementById("pickLine");

let x1 = 0;
let y1 = 0;
let x2 = 0;
let y2 = 0;
let targetValue;

function updateParameters() {
    c.strokeStyle = inputColor.value;
    c.lineWidth = inputWeight.value;
}

for (let i = 1; i < inputs.length; i++) {
    let temp = inputs[i];

    temp.addEventListener("click", function (event) {
        targetValue = event.target.value;
    });
}

function startDraw(event) {
    x1 = event.clientX;
    y1 = event.clientY;
}

function finishDraw(event) {
    x2 = event.clientX;
    y2 = event.clientY;
    updateParameters();
    startFunc(targetValue);
}

canvas.addEventListener("mousedown", startDraw);
canvas.addEventListener("mouseup", finishDraw);

function startFunc(targetValue) {

    switch (targetValue) {
        case "Rectangle":
            drawRect();
            break;

        case "Rounded Rectangle":
            drawRoundRect();
            break;

        case "Line":
            drawLine();
            break;

        case "Oval":
            drawOval();
            break;

        case "Ellipse":
            drawEllipse();
            break;

        default:
            break;
    }

}

function drawEllipse() {
    if (x2 < x1) {
        let x = x1;
        x1 = x2;
        x2 = x;
    }
    if (y2 < y1) {
        let y = y1;
        y1 = y2;
        y2 = y;
    }
    // Запоминаем положение системы координат (CК) и масштаб
    c.save();
    c.beginPath();
    // находим полуоси
    let a = (x2 - x1) / 2;
    let b = (y2 - y1) / 2;

    // Переносим СК в центр будущего эллипса
    c.translate((x1 + x2) / 2, (y1 + y2) / 2);

    // Масштабируем по х.
    c.scale(a / b, 1);

    // Рисуем окружность, которая благодаря масштабированию станет эллипсом
    c.arc(0, 0, b, 0, Math.PI * 2, true);

    // Восстанавливаем СК и масштаб
    c.restore();

    c.closePath();
    c.stroke();
}

function drawRect() {
    c.strokeRect(x1, y1, x2 - x1, y2 - y1);
    c.stroke();
}

function drawRoundRect() {
    const radius = 10;
    if (x2 < x1) {
        let x = x1;
        x1 = x2;
        x2 = x;
    }
    if (y2 < y1) {
        let y = y1;
        y1 = y2;
        y2 = y;
    }
    c.beginPath();
    c.moveTo(x1, y1 + radius);
    c.lineTo(x1, y2 - radius);
    c.quadraticCurveTo(x1, y2, x1 + radius, y2);
    c.lineTo(x2 - radius, y2);
    c.quadraticCurveTo(x2, y2, x2, y2 - radius);
    c.lineTo(x2, y1 + radius);
    c.quadraticCurveTo(x2, y1, x2 - radius, y1);
    c.lineTo(x1 + radius, y1);
    c.quadraticCurveTo(x1, y1, x1, y1 + radius);
    c.stroke();
}

function drawLine() {
    c.beginPath();
    c.moveTo(x1, y1);
    c.lineTo(x2, y2);
    c.stroke();
}

function drawOval() {
    let xMid = (x2 + x1) / 2;
    let yMid = (y2 + y1) / 2;
    c.beginPath();
    c.moveTo(x1, yMid);
    c.quadraticCurveTo(x1, y1, xMid, y1);
    c.quadraticCurveTo(x2, y1, x2, yMid);
    c.quadraticCurveTo(x2, y2, xMid, y2);
    c.quadraticCurveTo(x1, y2, x1, yMid);
    c.stroke();
}
